const canvas = document.querySelector("#jsCanvas");
const colors = document.querySelectorAll(".jsColor");
const range = document.querySelector("#jsRange");
const mode = document.querySelector("#jsMode");
const save = document.querySelector("#jsSave");
const reset = document.querySelector("#jsReset");
const eraser = document.querySelector(".eraser");
const ctx = canvas.getContext("2d");

const INITIAL_COLOR = "#2c2c2c";
const CANVAS_SIZE = 700;
canvas.width = CANVAS_SIZE;
canvas.height = CANVAS_SIZE;

ctx.fillStyle = "white";
ctx.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);
ctx.lineWidth = 2.5;
ctx.strokeStyle = INITIAL_COLOR;
ctx.fillStyle = INITIAL_COLOR;

let isPainting = false;
let isFilling = false;
let currentBackgroundColor = 'white';

const onMouseMove = (e) => {
  const x = e.offsetX;
  const y = e.offsetY;
  if (!isPainting || isFilling) {
    ctx.beginPath();
    ctx.moveTo(x, y);
  } else {
    ctx.lineTo(x, y);
    ctx.stroke();
  }
};

const onMouseDown = () => {
  isPainting = true;
};

const handleCanvasClick = (color) => {
  if (isFilling) {
    currentBackgroundColor = color;
    ctx.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);
    handleModeClick();
  }
};

colors.forEach((colorBtn) => {
  colorBtn.setAttribute("active", false);

  colorBtn.addEventListener("click", (event) => {
    resetStyles(colors);
    selectLineColor(event);
  });
});

const handleModeClick = () => {
  if (isFilling) {
    isFilling = false;
    mode.innerText = "Заливка";
  } else {
    isFilling = true;
    mode.innerText = "Рисование";
  }
};

const resetStyles = (arrayOfElements) => {
  arrayOfElements.forEach((element) => {
    element.setAttribute("active", false);
    element.style.transform = "translateY(0)";
  });
};

const selectLineColor = (event) => {
  event.target.setAttribute("active", true);
  event.target.style.transform = "translateY(-10px) scale(1.2)";

  const btnColor = event.target.style.backgroundColor;
  ctx.strokeStyle = btnColor;

  if (event.target.classList.contains("eraser")) {
    ctx.strokeStyle = currentBackgroundColor;
  btnColor = currentBackgroundColor;

  } else {
    ctx.fillStyle = btnColor;
  }
};

const changeStrokeWidth = (event) => {
  const rangeValue = event.target.value;
  ctx.lineWidth = rangeValue;
};

const stopPainting = () => (isPainting = false);
const startPainting = () => (isPainting = true);

const handleContextMenu = (e) => e.preventDefault();

const handleSaveImage = (e) => {
  const image = canvas.toDataURL();
  const link = document.createElement("a");
  link.href = image;
  link.download = "Paint Js (Export)";
  link.click();
  link.remove();
};

const handleResetImage = () => {
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);
};

if (canvas) {
  canvas.addEventListener("mousemove", onMouseMove);
  canvas.addEventListener("mousedown", onMouseDown);
  canvas.addEventListener("mouseup", stopPainting);
  canvas.addEventListener("mouseleave", stopPainting);
  canvas.addEventListener("click", () => handleCanvasClick(ctx.fillStyle));
  canvas.addEventListener("contextmenu", handleContextMenu);
}

if (mode) {
  mode.addEventListener("click", handleModeClick);
}

if (range) {
  range.addEventListener("input", (event) => changeStrokeWidth(event));
}

if (save) {
  save.addEventListener("click", handleSaveImage);
}

if (reset) {
  reset.addEventListener("click", handleResetImage);
}
